﻿using Windows.UI.Xaml;

namespace MagicMirror.UWP.Controllers
{
    public static class TimerController
    {
        public static void RegisterModel(BaseModel model)
        {
            var timer = new DispatcherTimer();
            timer.Interval = model.Interval;
            timer.Tick += model.TimerTick;
            timer.Start();
        }
    }
}