﻿using System;

namespace MagicMirror.UWP
{
    public abstract class BaseModel
    {
        public abstract TimeSpan Interval { get; }

        protected abstract void Update();

        internal void TimerTick(object sender, object e)
        {
            Update();
        }
    }
}