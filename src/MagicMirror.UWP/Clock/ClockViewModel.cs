﻿using MagicMirror.UWP.Annotations;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MagicMirror.UWP.Clock
{
    public class ClockViewModel : INotifyPropertyChanged
    {
        private string _currentTime = "12:23";
        private ClockModel _model;

        [NotNull]
        public string CurrentTime
        {
            get => _currentTime;
            private set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                if (_currentTime == value) return;
                _currentTime = value;
                OnPropertyChanged();
            }
        }

        internal void Initialize(ClockModel model)
        {
            this._model = model;
            UpdateTime();
        }

        private void UpdateTime()
        {
            CurrentTime = _model.CurrentTime.ToString("T");
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}