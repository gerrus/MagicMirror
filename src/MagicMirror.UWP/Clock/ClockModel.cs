﻿using System;

namespace MagicMirror.UWP.Clock
{
    public class ClockModel : BaseModel
    {
        private DateTime _currentTime;

        public DateTime CurrentTime
        {
            get => _currentTime;
            set => _currentTime = value;
        }

        public override TimeSpan Interval => TimeSpan.FromSeconds(1);

        protected override void Update()
        {
            CurrentTime = DateTime.Now;
        }
    }
}